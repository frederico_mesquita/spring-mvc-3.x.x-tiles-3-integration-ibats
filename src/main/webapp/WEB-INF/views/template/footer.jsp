<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div align="center">
	<ul style="list-style:none;line-height:28px;">
	    <li>
		    <spring:url value="/home" var="homeUrl" htmlEscape="true" />
		        <a href="${homeUrl}">Home</a>
	    </li>
	</ul>
</div>