package br.com.papodecafeteria.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.papodecafeteria.dao.IBatsDAO;
import br.com.papodecafeteria.model.UserJPA;

@Controller
public class SpringTilesController {
	private static Logger l = Logger.getLogger(SpringTilesController.class.getName());
	
    @RequestMapping(value="/home")
    public String index() {
        return "index";
    }
	
    @RequestMapping(value="/addUser")
    public String addUser() {
        return "addUser";
    }
    
    @RequestMapping(value="/view")
    public ModelAndView viewPersons(Model model) {
    	ModelAndView pModelAndView = null;
    	try {
    		Map<String, List<UserJPA>> persons = new HashMap<String, List<UserJPA>>();
            persons.put("persons", IBatsDAO.getAll());
            pModelAndView = new ModelAndView("personList", 	persons);
    	} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
        
        return pModelAndView;
    }
    
	@RequestMapping("/newuser")
	public String addUserForm(){
		return "adduserform";
	}

	@RequestMapping("/add")
	public String addUser(UserJPA pUserJPA){
		try{
			IBatsDAO.insert(pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return "redirect:view";
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView editUser(@PathVariable("id")int pId, Model pModel){
		ModelAndView pModelAndView = null;
		try{
			pModelAndView = new ModelAndView("editform", "userJPA", IBatsDAO.getById(pId));
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pModelAndView;
	}

	@RequestMapping("/edit/updateUser")
	public String updateUser(UserJPA pUserJPA){
		try{
			IBatsDAO.update(pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return "redirect:/view";
	}

	@RequestMapping("/delete/{id}")
	public ModelAndView deleteUser(@PathVariable("id")int pId){
		ModelAndView pModelAndView = null;
		try{
			UserJPA pUserJpa = new UserJPA();
			pUserJpa.setId(pId);
			IBatsDAO.delete(pId);
			pModelAndView = new ModelAndView("redirect:/view", "lista", IBatsDAO.getAll());
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pModelAndView;
	}
}
