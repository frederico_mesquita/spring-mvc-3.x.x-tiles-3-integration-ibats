package br.com.papodecafeteria.model;

public class UserJPA {
	private int id = 0;
	private String name = "";
	private String password = "";
	private String sex = "";
	private String email = "";
	private String country= "";
	
	public UserJPA(){}
	
	public UserJPA(String pName, String pPassword, String pSex, String pCountry){
		this.name = pName;
		this.password = pPassword;
		this.sex = pSex;
		this.country = pCountry;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSex() {
		return sex;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
}
