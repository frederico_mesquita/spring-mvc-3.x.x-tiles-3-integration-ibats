package br.com.papodecafeteria.dao;

import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import br.com.papodecafeteria.model.UserJPA;

public class IBatsDAO {
	
	private static Logger l = Logger.getLogger(IBatsDAO.class.getName());
	
	private static SqlMapClient getSqlMapClient(){
		SqlMapClient smc = null;
		try {
			Reader rd = Resources.getResourceAsReader("SqlMapConfig.xml");
			smc = SqlMapClientBuilder.buildSqlMapClient(rd);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return smc;
	}
	
	public static UserJPA getById(int pId){
		UserJPA pUserJPA = null;
		try {
			pUserJPA = (UserJPA) getSqlMapClient().queryForObject("UserJPA.useResultMap", pId);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return pUserJPA;
	}
	
	public static List<UserJPA> getAll(){
		List<UserJPA> lstUserJPA = null;
		try {
			lstUserJPA = (List<UserJPA>) getSqlMapClient().queryForList("UserJPA.getAll", null);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return lstUserJPA;
	}
	
	public static void delete(int pId){
		try{
			getSqlMapClient().delete("UserJPA.delete", pId);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void update(UserJPA pUserJPA){
		try{
			getSqlMapClient().update("UserJPA.update", pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
	
	public static void insert(UserJPA pUserJPA){
		try{
			getSqlMapClient().insert("UserJPA.insert", pUserJPA);
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return;
	}
}
